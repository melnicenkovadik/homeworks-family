package hw10;

import com.company.homeworks.homework10.entity.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FamilyTest {

    @Test
    void addChildTest() {
        Family family = new Family(new Man(), new Woman());
        Human child = new Man();
        Human child2 = new Man();
        family.addChild(child);

        assertEquals(1, family.getChildren().size());

        family.addChild(child2);
        assertEquals(2, family.getChildren().size());
    }

    @Test
    void deleteChildTest() {
        Family family = new Family(new Human(), new Human());
        Human child = new Human("Vadim", "Tartakovsky", "14/05/1999");
        Human child2 = new Human("Vadim", "Tartakovsky", "17/07/2002");
        family.addChild(child);
        family.addChild(child2);


        family.deleteChild(0);
        family.deleteChild(-1);
        family.deleteChild(5);

        family.deleteChild(0);

        System.out.println();

        System.out.println(family.getChildren());

        assertEquals(0, family.getChildren().size());

    }

    @Test
    void countFamilyTest() {
        Family family = new Family(new Human(), new Human());
        assertEquals(2, family.countFamily());

        family.addChild(new Human());
        family.addChild(new Human());

        assertEquals(4, family.countFamily());
        family.addChild(new Human());
        family.addChild(new Human());
        family.addChild(new Human());
        family.addChild(new Human());
        family.deleteChild(0);

        assertEquals(7, family.countFamily());
        family.addChild(new Human());
        family.addChild(new Human());

        assertEquals(9, family.countFamily());
    }

    @Test
    void toStringTest() {
        Family family = new Family(new Human(), new Human());
        System.out.println(family);
        assertEquals(family.toString(),
                "Family{" +
                        "\n\nmother="+
                        "\nHuman{" +
                        "\nname='" + "null" + '\'' +
                        ", \nsurname='" + "null" + '\'' +
                        ", \nbirthDate=" + "01/01/1970" +
                        ", \niq=" + "0" +
                        ", \nschedule=" + "{}" +
                        '}' +
                        ", \n\nfather=" +
                        "\nHuman{" +
                        "\nname='" + "null" + '\'' +
                        ", \nsurname='" + "null" + '\'' +
                        ", \nbirthDate=" + "01/01/1970" +
                        ", \niq=" + "0" +
                        ", \nschedule=" + "{}" +
                        '}' +
                        ", \n\nchildren=" + "[]" +
                        '}');
    }
}