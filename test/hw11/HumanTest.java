package hw11;

import com.company.homeworks.homework11.entity.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HumanTest {

    @Test
    void setFamilyTest() {
        Human father = new Human();
        Family family = new Family(father, new Human());

        assertEquals(family, father.getFamily());
    }


    @Test
    void testToString() {
        Human human = new Human("Vadim", "Tartakovsky", "14/05/2020");

        assertEquals(human.toString(), "\nHuman{" +
                "\nname='" + "Vadim" + '\'' +
                ", \nsurname='" + "Tartakovsky" + '\'' +
                ", \nbirthDate=" + "14/05/2020" +
                ", \niq=" + "0" +
                ", \nschedule=" + "{}" +
                '}');
    }
}