package hw13;

import com.company.homeworks.homework13.entity.*;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PetTest {

    @Test
    void testToString() {
        Set<String> habits = new HashSet<>();

        for (int i = 1; i <= 50; i++) {
            habits.add("Habit #" + i);
        }

        Pet pet = new DomesticCat("boss", (short) 5, (short) 57, habits);

        pet.respond();

        assertEquals(50, pet.getHabits().size());

    }
}