package hw5;

import com.company.homeworks.homework5.Family;
import com.company.homeworks.homework5.Human;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    @Test
    void addChildTest() {
        Family family = new Family(new Human(), new Human());
        Human child = new Human();
        family.addChild(child);

        assertEquals(1, family.getChildren().length);
    }

    @Test
    void deleteChildTest() {
        Family family = new Family(new Human(), new Human());
        Human child = new Human("Vadim", "Tartakovsky", (short) 1999);
        Human child2 = new Human("Vadim", "Tartakovsky", (short) 2003);
        family.addChild(child);
        family.addChild(child2);

        System.out.println("2 child " + Arrays.deepToString(family.getChildren()));

        assertEquals(1999, family.getChildren()[0].getYear());
        assertEquals(2003, family.getChildren()[1].getYear());

        family.deleteChild(0);

        System.out.println();
        System.out.println("1 child 2003 year " + Arrays.deepToString(family.getChildren()));

        assertEquals(2003, family.getChildren()[0].getYear());

        family.deleteChild(0);

        System.out.println();
        System.out.println("0 child " + Arrays.deepToString(family.getChildren()));

        assertEquals(0, family.getChildren().length);

    }
}