package hw6;

import com.company.homeworks.homework6.entity.Pet;
import com.company.homeworks.homework6.enums.Species;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {

    @Test
    void testToString() {
        Pet pet = new Pet(Species.CAT, "boss", (short) 5, (short) 57, new String[]{"habit 1"});

        assertEquals(pet.toString(), Species.CAT.nameSpec() + "{" +
                "species='" + Species.CAT.nameSpec() + '\'' +
                ", nickname='" + "boss" + '\'' +
                ", age=" + "5" +
                ", trickLevel=" + "57" +
                ", habits=" + "[habit 1]" +
                '}');

    }
}