package hw7;

import com.company.homeworks.homework7.entity.Family;
import com.company.homeworks.homework7.entity.Human;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FamilyTest {

    @Test
    void addChildTest() {
        Family family = new Family(new Human(), new Human());
        Human child = new Human();
        Human child2 = new Human();
        family.addChild(child);

        assertEquals(1, family.getChildren().length);

        family.addChild(child2);
        assertEquals(2, family.getChildren().length);
    }

    @Test
    void deleteChildTest() {
        Family family = new Family(new Human(), new Human());
        Human child = new Human("Vadim", "Tartakovsky", (short) 1999);
        Human child2 = new Human("Vadim", "Tartakovsky", (short) 2003);
        family.addChild(child);
        family.addChild(child2);

        System.out.println("2 child " + Arrays.deepToString(family.getChildren()));

        assertEquals(1999, family.getChildren()[0].getYear());
        assertEquals(2003, family.getChildren()[1].getYear());

        family.deleteChild(0);
        family.deleteChild(-1);
        family.deleteChild(5);


        System.out.println();
        System.out.println("1 child 2003 year " + Arrays.deepToString(family.getChildren()));

        assertEquals(2003, family.getChildren()[0].getYear());

        family.deleteChild(0);

        System.out.println();
        System.out.println("0 child " + Arrays.deepToString(family.getChildren()));

        assertEquals(0, family.getChildren().length);

    }

    @Test
    void countFamilyTest() {
        Family family = new Family(new Human(), new Human());
        assertEquals(2, family.countFamily());

        family.addChild(new Human());
        family.addChild(new Human());

        assertEquals(4, family.countFamily());
        family.addChild(new Human());
        family.addChild(new Human());
        family.addChild(new Human());
        family.addChild(new Human());
        family.deleteChild(0);

        assertEquals(7, family.countFamily());
        family.addChild(new Human());
        family.addChild(new Human());

        assertEquals(9, family.countFamily());
    }

    @Test
    void toStringTest() {
        Family family = new Family(new Human(), new Human());

        assertEquals(family.toString(),
                "Family{" +
                        "\n\nmother="+
                        "\nHuman{" +
                        "\nname='" + "null" + '\'' +
                        ", \nsurname='" + "null" + '\'' +
                        ", \nyear=" + "0" +
                        ", \niq=" + "0" +
                        ", \nschedule=" + "[[null, null], [null, null], [null, null], [null, null], [null, null], [null, null], [null, null]]" +
                        '}' +
                        ", \n\nfather=" +
                        "\nHuman{" +
                        "\nname='" + "null" + '\'' +
                        ", \nsurname='" + "null" + '\'' +
                        ", \nyear=" + "0" +
                        ", \niq=" + "0" +
                        ", \nschedule=" + "[[null, null], [null, null], [null, null], [null, null], [null, null], [null, null], [null, null]]" +
                        '}' +
                        ", \n\nchildren=" + "[]" +
                        ", \n\npet=" + "null" +
                        '}');
    }


}