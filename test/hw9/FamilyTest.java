package hw9;

import com.company.homeworks.homework8.entity.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FamilyTest {

    @Test
    void addChildTest() {
        Family family = new Family(new Man(), new Woman());
        Human child = new Man();
        Human child2 = new Man();
        family.addChild(child);

        assertEquals(1, family.getChildren().size());

        family.addChild(child2);
        assertEquals(2, family.getChildren().size());
    }

    @Test
    void deleteChildTest() {
        Family family = new Family(new Human(), new Human());
        Human child = new Human("Vadim", "Tartakovsky", (short) 1999);
        Human child2 = new Human("Vadim", "Tartakovsky", (short) 2003);
        family.addChild(child);
        family.addChild(child2);


        family.deleteChild(0);
        family.deleteChild(-1);
        family.deleteChild(5);

        family.deleteChild(0);

        System.out.println();

        System.out.println(family.getChildren());

        assertEquals(0, family.getChildren().size());

    }

    @Test
    void countFamilyTest() {
        Family family = new Family(new Human(), new Human());
        assertEquals(2, family.countFamily());

        family.addChild(new Human());
        family.addChild(new Human());

        assertEquals(4, family.countFamily());
        family.addChild(new Human());
        family.addChild(new Human());
        family.addChild(new Human());
        family.addChild(new Human());
        family.deleteChild(0);

        assertEquals(7, family.countFamily());
        family.addChild(new Human());
        family.addChild(new Human());

        assertEquals(9, family.countFamily());
    }

    @Test
    void toStringTest() {
        Family family = new Family(new Human(), new Human());

        assertEquals(family.toString(),
                "Family{" +
                        "\n\nmother="+
                        "\nHuman{" +
                        "\nname='" + "null" + '\'' +
                        ", \nsurname='" + "null" + '\'' +
                        ", \nyear=" + "0" +
                        ", \niq=" + "0" +
                        ", \nschedule=" + "{}" +
                        '}' +
                        ", \n\nfather=" +
                        "\nHuman{" +
                        "\nname='" + "null" + '\'' +
                        ", \nsurname='" + "null" + '\'' +
                        ", \nyear=" + "0" +
                        ", \niq=" + "0" +
                        ", \nschedule=" + "{}" +
                        '}' +
                        ", \n\nchildren=" + "[]" +
                        ", \n\npet=" + "null" +
                        '}');
    }


}