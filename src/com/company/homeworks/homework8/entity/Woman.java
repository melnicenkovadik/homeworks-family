package com.company.homeworks.homework8.entity;

import java.util.Map;
import java.util.Random;

public final class Woman extends Human {
    public Woman(String name, String surname, short year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, short year, Human mother, Human father) {
        super(name, surname, year, mother, father);
    }

    public Woman(String name, String surname, short year, short iq, Human mother, Human father, Map<String, String> schedule) {
        super(name, surname, year, iq, mother, father, schedule);
    }

    public Woman() {
    }

    @Override
    public void greetPet() {
        if (this.getFamily().getPet() != null) {
            System.out.println("Здравствуй, " + this.getFamily().getPet().getNickname());
        } else {
            System.out.println("В Вашей семье нет питомца! :(");
        }
    }

    public void makeUp() {
        Random rand = new Random();

        float randNum = rand.nextFloat();

        if (randNum > 0.5) {
            System.out.println("Мейкап сделан успешно.");
        } else {
            System.out.println("Увы, нужно переделать.");
        }
    }

}
