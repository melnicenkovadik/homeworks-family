package com.company.homeworks.homework10.controller;

import com.company.homeworks.homework10.entity.*;
import com.company.homeworks.homework10.service.FamilyService;

import java.util.List;

public class FamilyController {
    FamilyService services = new FamilyService();

    public Family getFamilyByIndex(int index) {
        return services.getFamilyByIndex(index);
    }

    public void displayAllFamilies() {
        services.displayAllFamilies();
    }

    public List<Family> getAllFamilies() {
        return services.getAllFamilies();
    }

    public void getFamiliesBiggerThan(int countPerson) {
        services.getFamiliesBiggerThan(countPerson);
    }

    public void getFamiliesLessThan(int countPerson) {
        services.getFamiliesLessThan(countPerson);
    }

    public int countFamiliesWithMemberNumber(int countPerson) {
        return services.countFamiliesWithMemberNumber(countPerson);
    }


    public Family createNewFamily(Human firstPerson, Human secondPerson) {
        return services.createNewFamily(firstPerson, secondPerson);
    }

    public Family bornChild(Family family, String nameBoy, String nameGirl) {
        return services.bornChild(family, nameBoy, nameGirl);
    }

    public Family adoptChild(Family family, Human child) {
        return services.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThan(int age) {
        services.deleteAllChildrenOlderThan(age);
    }

    public boolean deleteFamilyByIndex(int index) {
        return services.deleteFamilyByIndex(index);
    }

    public int count() {
        return services.count();
    }

    public List<Pet> getPets(int indexFamily) {
        return services.getPets(indexFamily);
    }

    public Family addPet(int indexFamily, Pet pet) {
        return services.addPet(indexFamily, pet);
    }
}
