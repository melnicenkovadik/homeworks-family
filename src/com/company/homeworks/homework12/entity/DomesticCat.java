package com.company.homeworks.homework12.entity;

import com.company.homeworks.homework12.enums.Species;

import java.util.Set;

public class DomesticCat extends Pet implements Foulable {
    public DomesticCat(String nickname) {
        super(nickname);
        this.setSpecies(Species.DOMESTIC_CAT);
    }

    public DomesticCat(String nickname, short age, short trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.DOMESTIC_CAT);
    }

    public DomesticCat() {
        this.setSpecies(Species.DOMESTIC_CAT);
    }

    @Override
    public void foul() {
        System.out.println("Нужно замести следы...");
    }

    @Override
    public void respond() {
        System.out.println("MEEEEAOOOUU");
    }
}
