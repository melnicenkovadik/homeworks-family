package com.company.homeworks.homework12.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private long birthDate;
    private short iq;
    private Human mother;
    private Human father;
    private Map<String, String> schedule = new HashMap<>();
    private Family family;

    public void greetPet() {
        System.out.println("Привет, " + this.family.getPet().iterator().next().getNickname());
    }

    public void describePet() {
        System.out.println("У меня есть " + this.family.getPet().iterator().next().getSpecies() + ", ему " + this.family.getPet().iterator().next().getAge() + " лет, он " + (this.family.getPet().iterator().next().getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый"));
    }

    public String describeAge() {
        int years, months, days;
        years = Period.between(LocalDate.ofInstant(Instant.ofEpochMilli(this.birthDate), ZoneId.systemDefault()), LocalDate.now()).getYears();
        months = Period.between(LocalDate.ofInstant(Instant.ofEpochMilli(this.birthDate), ZoneId.systemDefault()), LocalDate.now()).getMonths();
        days = Period.between(LocalDate.ofInstant(Instant.ofEpochMilli(this.birthDate), ZoneId.systemDefault()), LocalDate.now()).getDays();

        return String.format("Лет - %d, Месяцев - %d, Дней - %d", years, months, days);
    }

    public int getAge() {
        return Period.between(LocalDate.ofInstant(Instant.ofEpochMilli(this.birthDate), ZoneId.systemDefault()), LocalDate.now()).getYears();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public short getIq() {
        return iq;
    }

    public void setIq(short iq) {
        this.iq = iq;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String prettyFormat() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + new SimpleDateFormat("dd/MM/yyyy").format(this.birthDate) +
                ", iq=" + iq +
                ", schedule=" + schedule.toString() +
                '}';
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate='" + new SimpleDateFormat("dd/MM/yyyy").format(this.birthDate) + '\'' +
                ", iq=" + iq +
                ", schedule=" + schedule.toString() +
                '}';
    }

    public Human(String name, String surname, String birthDate) {
        this.name = name;
        this.surname = surname;
        try {
            this.birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(birthDate).getTime();
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }
    }

    public Human(String name, String surname, String birthDate, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        try {
            this.birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(birthDate).getTime();
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }
        this.mother = mother;
        this.father = father;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(mother, human.mother) &&
                Objects.equals(father, human.father) &&
                Objects.equals(schedule, human.schedule) &&
                Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, iq, mother, father, schedule, family);
    }

    public Human(String name, String surname, String birthDate, short iq, Human mother, Human father, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        try {
            this.birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(birthDate).getTime();
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }
        this.iq = iq;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    public Human() {
    }

    @Override
    protected void finalize() {
        System.out.println("Удаляется обьект - " + this);
    }
}
