package com.company.homeworks.homework12.service;

import com.company.homeworks.homework12.dao.CollectionFamilyDao;
import com.company.homeworks.homework12.dao.FamilyDao;
import com.company.homeworks.homework12.entity.*;
import com.company.homeworks.homework12.exceptions.FamilyOverflowException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class FamilyService {
    private FamilyDao<Family> familyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        if (this.familyDao.getAllFamilies().size() > 0) {
            this.familyDao.getAllFamilies().stream()
                    .forEach(family -> {
                        System.out.println(family.prettyFormat());
                    });
        } else {
            System.out.println("\nСписок семей пустой.");
        }
    }

    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index < this.count()) {
            return familyDao.getFamilyByIndex(index);
        } else {
            return null;
        }
    }

    public void getFamiliesBiggerThan(int countPerson) {
        this.familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() > countPerson)
                .forEach(family -> {
                    System.out.println(family.prettyFormat());
                });
    }

    public void getFamiliesLessThan(int countPerson) {
        this.familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() < countPerson)
                .forEach(family -> {
                    System.out.println(family.prettyFormat());
                });
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public int countFamiliesWithMemberNumber(int countPerson) {
        return this.familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() == countPerson)
                .collect(Collectors.toList()).size();
    }


    public Family createNewFamily(Human firstPerson, Human secondPerson) {
        Family newFamily = new Family(firstPerson, secondPerson);
        boolean noErr = this.familyDao.saveFamily(newFamily);
        return noErr ? newFamily : null;
    }

    public Family bornChild(Family family, String nameBoy, String nameGirl) {
        if(family.countFamily() > 5) {
            throw new FamilyOverflowException();
        }

        Random rand = new Random();
        float randNum = rand.nextFloat();

        if (randNum > 0.5) {
            family.addChild(new Man(nameBoy, family.getFather().getSurname(), new SimpleDateFormat("dd/MM/yyyy").format(new GregorianCalendar().getTimeInMillis())));
        } else {
            family.addChild(new Woman(nameGirl, family.getFather().getSurname(), new SimpleDateFormat("dd/MM/yyyy").format(new GregorianCalendar().getTimeInMillis())));
        }

        this.familyDao.saveFamily(family);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        if(family.countFamily() > 5) {
            throw new FamilyOverflowException();
        }

        family.addChild(child);
        this.familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThan(int age) {
        this.familyDao.getAllFamilies().stream()
                .forEach(family -> {
                    family.getChildren().stream()
                            .filter(child -> child.getAge() > age);
                });
    }

    public int count() {
        return this.familyDao.getAllFamilies().size();
    }

    public List<Pet> getPets(int indexFamily) {
        List<Pet> pets = new ArrayList<>();

        for (int i = 0; i < this.familyDao.getAllFamilies().size(); i++) {
            if (this.familyDao.getFamilyByIndex(i).getPet() != null) {
                for (int j = 0; j < this.familyDao.getFamilyByIndex(i).getPet().size(); j++) {
                    pets.add((Pet) this.familyDao.getFamilyByIndex(i).getPet().toArray()[j]);
                }
            }
        }

        return pets;
    }

    public Family addPet(int indexFamily, Pet pet) {
        this.familyDao.getFamilyByIndex(indexFamily).addPet(pet);
        this.familyDao.saveFamily(this.familyDao.getFamilyByIndex(indexFamily));
        return this.familyDao.getFamilyByIndex(indexFamily);
    }
}
