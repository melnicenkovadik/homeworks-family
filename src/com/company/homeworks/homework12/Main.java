package com.company.homeworks.homework12;

import com.company.homeworks.homework12.entity.*;
import com.company.homeworks.homework12.controller.FamilyController;

import java.util.*;

public class Main {
    private static FamilyController controller = new FamilyController();


    public static void main(String[] args) {
        int command;
        Scanner scan = new Scanner(System.in);

        do {
            printMenu();
            System.out.print("\nВведите код действия: ");

            while (!scan.hasNextInt()) {
                System.out.println("Ошибка действия!");
                scan.next();
                System.out.print("\nВведите код действия: ");
            }
            command = scan.nextInt();

            switch (command) {
                case 1:
                    createRandomData(200);
                    break;
                case 2:
                    controller.displayAllFamilies();
                    break;
                case 3:
                    System.out.print("\nВведите кол-во членов: ");
                    while (!scan.hasNextInt()) {
                        System.out.println("Ошибка действия!");
                        scan.next();
                        System.out.print("\nВведите кол-во членов: ");
                    }
                    int countBigger = scan.nextInt();
                    controller.getFamiliesBiggerThan(countBigger);
                    break;
                case 4:
                    System.out.print("\nВведите кол-во членов: ");
                    while (!scan.hasNextInt()) {
                        System.out.println("Ошибка действия!");
                        scan.next();
                        System.out.print("\nВведите кол-во членов: ");
                    }
                    int countLess = scan.nextInt();
                    controller.getFamiliesLessThan(countLess);
                    break;
                case 5:
                    System.out.print("\nВведите кол-во членов: ");
                    while (!scan.hasNextInt()) {
                        System.out.println("Ошибка действия!");
                        scan.next();
                        System.out.print("\nВведите кол-во членов: ");
                    }
                    int countPersons = scan.nextInt();
                    System.out.printf("\nКол-во семей = %d\n", controller.countFamiliesWithMemberNumber(countPersons));
                    break;
                case 6:
                    System.out.print("Введите имя отца: ");
                    String nameFather = scan.next();
                    System.out.print("Введите фамилию отца: ");
                    String surnameFather = scan.next();
                    System.out.print("Введите дату рождения отца в формате \"дд/ММ/гггг\": ");
                    String birthDateFather = scan.next();

                    System.out.print("\nВведите имя матери: ");
                    String nameMother = scan.next();
                    System.out.print("Введите фамилию матери: ");
                    String surnameMother = scan.next();
                    System.out.print("Введите дату рождения матери в формате \"дд/ММ/гггг\": ");
                    String birthDateMother = scan.next();

                    controller.createNewFamily(new Woman(nameMother, surnameMother, birthDateMother), new Man(nameFather, surnameFather, birthDateFather));
                    break;
                case 7:
                    System.out.print("\nВведите индекс семьи: ");
                    while (!scan.hasNextInt()) {
                        System.out.println("Ошибка действия!");
                        scan.next();
                        System.out.print("\nВведите индекс семьи: ");
                    }
                    int indexFamily = scan.nextInt();
                    System.out.println(controller.deleteFamilyByIndex(indexFamily) ? "\nУдаление успешное." : "\nОшибка при удалении.");
                    break;
                case 8:
                    int addChildCommand;

                    do {
                        System.out.println("\n1. Родить ребенка");
                        System.out.println("2. Усыновить ребенка");
                        System.out.println("0. Выйти в главное меню\n");

                        System.out.print("\nВведите код действия: ");
                        while (!scan.hasNextInt()) {
                            System.out.println("Ошибка действия!");
                            scan.next();
                            System.out.print("\nВведите код действия: ");
                        }
                        addChildCommand = scan.nextInt();

                        switch (addChildCommand) {
                            case 1:
                                int familyNumberToBorn;
                                String nameBoy, nameGirl;
                                System.out.print("\nВведите порядковый номер семьи: ");
                                while (!scan.hasNextInt()) {
                                    System.out.println("Ошибка!");
                                    scan.next();
                                    System.out.print("\nВведите порядковый номер семьи: ");
                                }
                                familyNumberToBorn = scan.nextInt();

                                System.out.print("\nВведите имя если будет мальчик: ");
                                nameBoy = scan.next();

                                System.out.print("\nВведите имя если будет девочка: ");
                                nameGirl = scan.next();

                                controller.bornChild(controller.getFamilyByIndex(familyNumberToBorn), nameBoy, nameGirl);
                                addChildCommand = 0;
                                break;
                            case 2:
                                int familyNumberToAdopt;
                                System.out.print("\nВведите порядковый номер семьи: ");
                                while (!scan.hasNextInt()) {
                                    System.out.println("Ошибка!");
                                    scan.next();
                                    System.out.print("\nВведите порядковый номер семьи: ");
                                }
                                familyNumberToAdopt = scan.nextInt();

                                System.out.print("\nВыберите пол ребенка: ");
                                int genderChild;
                                System.out.println("1. Мальчик");
                                System.out.println("2. Девочка");

                                System.out.println("\nВведите код пола ребенка: ");

                                do {
                                    while (!scan.hasNextInt()) {
                                        System.out.println("Ошибка!");
                                        scan.next();
                                        System.out.println("\nВведите код пола ребенка: ");
                                    }
                                    genderChild = scan.nextInt();
                                } while (genderChild < 1 && genderChild > 2);


                                System.out.print("\nВведите имя ребенка: ");
                                String nameChild = scan.next();
                                System.out.print("\nВведите фамилию ребенка: ");
                                String surnameChild = scan.next();
                                System.out.print("\nВведите дату рождения ребенка (дд/мм/гггг): ");
                                String birthDateChild = scan.next();

                                Human children;
                                if (genderChild == 1) {
                                    children = new Man(nameChild, surnameChild, birthDateChild);
                                } else if (genderChild == 2) {
                                    children = new Woman(nameChild, surnameChild, birthDateChild);
                                } else {
                                    children = new Man(nameChild, surnameChild, birthDateChild);

                                }

                                controller.adoptChild(controller.getFamilyByIndex(familyNumberToAdopt), children);
                                addChildCommand = 0;
                                break;
                            case 0:
                                break;
                            default:
                                System.out.println("\nОшибка действия.");
                        }
                    } while (addChildCommand != 0);
                    break;
                case 9:
                    System.out.print("\nВведите возраст: ");
                    int ageChildren = scan.nextInt();
                    controller.deleteAllChildrenOlderThan(ageChildren);
                    break;
                case 0:
                    break;
                default:
                    System.out.println("\nОшибка действия.\n");
            }

        } while (command != 0);
    }

    public static void printMenu() {
        System.out.println("\n1. Заполнить тестовыми данными");
        System.out.println("2. Отобразить весь список семей");
        System.out.println("3. Отобразить список семей, где количество людей больше заданного");
        System.out.println("4. Отобразить список семей, где количество людей меньше заданного");
        System.out.println("5. Подсчитать количество семей, где количество членов равно");
        System.out.println("6. Создать новую семью");
        System.out.println("7. Удалить семью по индексу семьи в общем списке");
        System.out.println("8. Редактировать семью по индексу семьи в общем списке ");
        System.out.println("9. Удалить всех детей старше возраста");
        System.out.println("0. Выход\n");
    }

    public static void createRandomData(int count) {
        List<String> manNames = Arrays.asList("Vadim", "Andrew", "Maks", "Anton", "Oleg", "Vlad");
        List<String> manLNames = Arrays.asList("Tartakovsky", "Petrov", "Stolyar", "Savchenko", "Zaycev");
        List<String> womanNames = Arrays.asList("Uliana", "Agata", "Anastasia", "Alice", "Ann", "Angelina");
        List<String> womanLNames = Arrays.asList("Kovalenko", "Petrova", "Zayceva", "Popova", "Ivanova");

        Random rand = new Random();

        for (int i = 0; i < 200; i++) {
            Collections.shuffle(manNames);
            Collections.shuffle(manLNames);
            Collections.shuffle(womanLNames);
            Collections.shuffle(womanNames);

            Human father = new Man(manNames.get(0), manLNames.get(0), String.format("%d/%d/%d", rand.nextInt((29 - 1) + 1) + 1, rand.nextInt((12 - 1) + 1) + 1, rand.nextInt((2003 - 1970) + 1) + 1970));
            Human mother = new Woman(womanNames.get(0), womanLNames.get(0), String.format("%d/%d/%d", rand.nextInt((29 - 1) + 1) + 1, rand.nextInt((12 - 1) + 1) + 1, rand.nextInt((2003 - 1970) + 1) + 1970));

            controller.createNewFamily(mother, father);
        }
    }
}
