package com.company.homeworks.homework4;

import java.util.Arrays;

public class Human {
    private String name;
    private String surname;
    private short year;
    private short iq;
    private Pet pet;
    private Human mother;
    private Human father;
    private String[][] schedule = new String[7][2];

    public void greetPet() {
        System.out.println("Привет, " + this.pet.getNickname());
    }

    public void describePet() {
        System.out.println("У меня есть " + this.pet.getSpecies() + ", ему " + this.pet.getAge() + " лет, он " + (this.pet.getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый"));
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public short getYear() {
        return year;
    }

    public short getIq() {
        return iq;
    }

    public Pet getPet() {
        return pet;
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    @Override
    public String toString() {
        return "\nHuman{" +
                "\nname='" + name + '\'' +
                ", \nsurname='" + surname + '\'' +
                ", \nyear=" + year +
                ", \niq=" + iq +
                ", \npet=" + (pet != null ? pet : "null") +
                ", \nmother=" + (mother != null ? mother : "null") +
                ", \nfather=" + (father != null ? mother : "null") +
                ", \nschedule=" + Arrays.deepToString(schedule) +
                '}';
    }

    public Human(String name, String surname, short year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, short year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, short year, short iq, Pet pet, Human mother, Human father, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    public Human() {
    }
}
