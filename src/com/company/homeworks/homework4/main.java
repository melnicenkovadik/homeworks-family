package com.company.homeworks.homework4;

public class main {

    public static void main(String[] args) {
        Human father = new Human("Петр", "Петров", (short) 1996);
        Human mother = new Human("Ульяна", "Петрова", (short) 1997);
        Human child = new Human("Вадим", "Петров", (short) 2010, mother, father);

        String[] petHabitsArray = {"1 habit", "2 habit"};

        Pet pet = new Pet("Собака", "Шарик", (short) 13, (short) 67, petHabitsArray);

        Human father1 = new Human("Петр", "Петров", (short) 1996);
        Human mother1 = new Human("Ульяна", "Петрова", (short) 1997);

        String[][] arraySchedule = {{"Занятия"}, {"Занятия"}, {"Занятия"}, {"Занятия"}, {"Занятия"}, {"Занятия"}, {"Занятия"}};

        Human child2 = new Human("Вадим", "Петров", (short) 2010, (short) 80, pet, mother1, father1, arraySchedule);

        System.out.println(father);
        System.out.println();
        System.out.println(mother);

        System.out.println(child2);

        System.out.println();

        child2.describePet();
        child2.greetPet();
        child2.getPet().eat();
        child2.getPet().foul();
        child2.getPet().respond();
    }
}
