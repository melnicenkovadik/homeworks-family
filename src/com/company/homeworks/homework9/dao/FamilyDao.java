package com.company.homeworks.homework9.dao;

import java.util.List;

public interface FamilyDao<F> {
    List<F> getAllFamilies();

    F getFamilyByIndex(int index);

    boolean deleteFamily(int index);

    boolean deleteFamily(F family);

    boolean saveFamily(F family);
}
