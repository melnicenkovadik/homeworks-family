package com.company.homeworks.homework9.entity;

import com.company.homeworks.homework9.enums.Species;
import com.company.homeworks.homework9.entity.Foulable;
import com.company.homeworks.homework9.entity.Pet;

import java.util.Set;

public class RoboCat extends Pet implements Foulable {
    public RoboCat(String nickname) {
        super(nickname);
        this.setSpecies(Species.ROBOCAT);
    }

    public RoboCat(String nickname, short age, short trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.ROBOCAT);
    }

    public RoboCat() {
        this.setSpecies(Species.ROBOCAT);
    }

    @Override
    public void foul() {
        System.out.println("Здравствуйте, собственно говоря...");
    }

    @Override
    public void respond() {
        System.out.println("efowfnosoi".getBytes().toString());
    }
}
