package com.company.homeworks.homework9;

import com.company.homeworks.homework9.controller.FamilyController;
import com.company.homeworks.homework9.entity.*;

public class Main {
    public static void main(String[] args) {
        FamilyController familyController = new FamilyController();

        Human vadim = new Man("Vadim", "Tartakovsky", (short) 17);
        Human uliana = new Woman("Uliana", "Kovalenko", (short) 16);
        Human adoptChild = new Man("Oleg", "Petrov", (short) 9);

        Family family = familyController.createNewFamily(uliana, vadim);

//        familyController.bornChild(family, "Vlad", "Vladislava");

//        familyController.adoptChild(family, adoptChild);

//        familyController.displayAllFamilies();
//        System.out.println(familyController.getAllFamilies());;

//        familyController.getFamiliesBiggerThan(5);
//        familyController.getFamiliesBiggerThan(2);

//        familyController.getFamiliesLessThan(2);
//        familyController.getFamiliesLessThan(5);

//        System.out.println(familyController.countFamiliesWithMemberNumber(4));
//        System.out.println(familyController.countFamiliesWithMemberNumber(3));

//        familyController.deleteFamilyByIndex(0);
//        familyController.deleteFamilyByIndex(1);

//        familyController.deleteAllChildrenOlderThan(9);

//        System.out.println(familyController.count());

//        familyController.addPet(0, new RoboCat("iCat"));
//        familyController.addPet(0, new Dog("iDog"));
//        System.out.println(familyController.getPets(0));

    }
}
