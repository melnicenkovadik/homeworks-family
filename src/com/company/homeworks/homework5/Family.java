package com.company.homeworks.homework5;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public void addChild(Human child) {
        child.setFamily(this);
        Human[] tempArr = new Human[children.length + 1];
        System.arraycopy(children, 0, tempArr, 0, children.length);
        tempArr[tempArr.length - 1] = child;
        children = tempArr;
    }

    public boolean deleteChild(int index) {
        boolean error = true;
        if (index < children.length && index >= 0 && children.length > 0) {
            Human[] tempArr = new Human[children.length - 1];
            for (int i = 0; i < children.length - 1; i++) {
                if (i >= index) {
                    children[i] = children[i + 1];
                }
            }

            System.arraycopy(children, 0, tempArr, 0, children.length - 1);
            children = tempArr;
            error = false;
        }
        return error;
    }


    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[0];

        mother.setFamily(this);
        father.setFamily(this);
    }

    public int countFamily() {
        return (mother != null ? 1 : 0) + (father != null ? 1 : 0) + (children.length) + (pet != null ? 1 : 0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Arrays.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    public String toString() {
        return "Family{" +
                "\n\nmother=" + (mother != null ? mother : null) +
                ", \n\nfather=" + (father != null ? father : null) +
                ", \n\nchildren=" + (children != null ? Arrays.toString(children) : null) +
                ", \n\npet=" + (pet != null ? pet.getNickname() : null) +
                '}';
    }
}
