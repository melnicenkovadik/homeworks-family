package com.company.homeworks.homework5;

public class main {

    public static void main(String[] args) {
        Human father = new Human("Петр", "Петров", (short) 1996);
        Human mother = new Human("Ульяна", "Петрова", (short) 1997);
        Human child = new Human("Вадим", "Петров", (short) 2010, mother, father);

        String[] petHabitsArray = {"1 habit", "2 habit"};

        Pet pet = new Pet("Собака", "Шарик", (short) 13, (short) 67, petHabitsArray);

        Human father1 = new Human("Петр", "Петров", (short) 1996);
        Human mother1 = new Human("Ульяна", "Петрова", (short) 1997);

        String[][] arraySchedule = {{"Пн", "Занятия"}, {"Вт", "Занятия"}, {"Ср", "Занятия"}, {"Чт", "Занятия"}, {"Пт", "Занятия"}, {"Сб", "Занятия"}, {"Вс", "Занятия"}};

        Human child2 = new Human("Вадим", "Пfwrwrqw", (short) 2010, (short) 80, mother1, father1, arraySchedule);

        Family family = new Family(mother, father);

        family.addChild(child2);
        family.addChild(child);

        family.deleteChild(0);
        System.out.println(family.deleteChild(1));

        System.out.println(family);

    }
}
