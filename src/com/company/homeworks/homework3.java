package com.company.homeworks;

import java.util.Scanner;

public class homework3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[][] taskList = new String[7][2];

        taskList[0][0] = "Sunday";
        taskList[0][1] = "do home work";
        taskList[1][0] = "Monday";
        taskList[1][1] = "go to courses; watch a film";

        boolean programWorked = true;

        do {
            System.out.print("\nPlease, input the day of the week: ");
            String textUserDay = scan.next();
            textUserDay = textUserDay.toLowerCase();
            switch (textUserDay) {
                case "monday":
                case "tuesday":
                case "wednesday":
                case "thursday":
                case "friday":
                case "saturday":
                case "sunday":
                    for (int i = 0; i < taskList.length; i++) {
                        if(taskList[i][0] != null) {
                            if(taskList[i][0].equalsIgnoreCase(textUserDay)) {
                                System.out.print("Your tasks for " + taskList[i][0].substring(0, 1).toUpperCase() + taskList[i][0].substring(1).toLowerCase() + ": ");
                                for (int j = 0; j < taskList[0].length; j++) {
                                    if(taskList[i][j] != null && j != 0) {
                                        System.out.print(taskList[i][j ] + "; ");
                                    }
                                }
                            }
                        }
                    }
                    System.out.println();
                    break;
                case "exit":
                    programWorked = false;
                    break;
                default:
                    System.out.println("\nSorry, I don't understand you, please try again.");
                    break;
            }
        } while (programWorked);
    }
}
