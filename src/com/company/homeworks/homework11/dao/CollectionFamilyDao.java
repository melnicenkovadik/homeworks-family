package com.company.homeworks.homework11.dao;

import com.company.homeworks.homework11.entity.Family;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao<Family> {
    private List<Family> familyCollection = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return new ArrayList<>(this.familyCollection);
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return this.familyCollection.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        int size = this.familyCollection.size();
        if (index >= 0 && index < size) {
            this.familyCollection.remove(index);
        }

        return size > this.familyCollection.size();
    }

    @Override
    public boolean deleteFamily(Family family) {
        return this.familyCollection.remove(family);
    }

    @Override
    public boolean saveFamily(Family family) {
        if (this.familyCollection.contains(family)) {
            this.familyCollection.set(this.familyCollection.indexOf(family), family);
        } else {
            return this.familyCollection.add(family);
        }

        return true;
    }
}
