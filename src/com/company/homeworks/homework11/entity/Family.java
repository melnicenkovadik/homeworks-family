package com.company.homeworks.homework11.entity;


import java.util.*;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children = new ArrayList<>();
    private Set<Pet> pet = new HashSet<>();

    public void addChild(Human child) {
        children.add(child);
    }

    public boolean deleteChild(int index) {
        if (index >= 0 && index < this.children.size()) {
            int tempSize = this.children.size();
            children.remove(index);
            return tempSize > children.size();
        } else {
            return false;
        }
    }

    public boolean deleteChild(Human child) {
        return children.remove(child);
    }


    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public Set<Pet> getPet() {
        return pet;
    }

    public boolean addPet(Pet pet) {
        return this.pet.add(pet);
    }

    public boolean removePet(Pet pet) {
        return this.pet.remove(pet);
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;

        mother.setFamily(this);
        father.setFamily(this);
    }

    public int countFamily() {
        return (mother != null ? 1 : 0) + (father != null ? 1 : 0) + (children.size()) + (pet.size());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Objects.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pet);
    }

    @Override
    public String toString() {
        return "Family{" +
                "\n\nmother=" + (mother != null ? mother : null) +
                ", \n\nfather=" + (father != null ? father : null) +
                ", \n\nchildren=" + (children != null ? children.toString() : null) +
                '}';
    }

    @Override
    protected void finalize () {
        System.out.println(this);
    }
}
