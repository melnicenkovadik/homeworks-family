package com.company.homeworks.homework11.entity;

import com.company.homeworks.homework11.enums.Species;

import java.util.Set;

public class Dog extends Pet implements Foulable {
    public Dog(String nickname) {
        super(nickname);
        this.setSpecies(Species.DOG);
    }

    public Dog(String nickname, short age, short trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.DOG);
    }

    public Dog() {
        this.setSpecies(Species.DOG);
    }

    @Override
    public void respond() {
        System.out.println("Здравствуйте, собственно говоря...");
    }

    @Override
    public void foul() {
        System.out.println("Нужно замести следы...");
    }
}
