package com.company.homeworks;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class homework2 {
    public static void main(String[] args) {
        Random rand = new Random();
        Scanner scan = new Scanner(System.in);

        boolean won = false;
        int countTry = 0;
        int sizeGameArea = 5;

        char[][] gameArea = new char[sizeGameArea][sizeGameArea];
        int randPosX = rand.nextInt(sizeGameArea);
        int randPosY = rand.nextInt(sizeGameArea);

        for (int i = 0; i < gameArea.length; i++) {
            Arrays.fill(gameArea[i], '-');
        }

        gameArea[randPosX][randPosY] = '1';

        do {
            printArea(gameArea, sizeGameArea);
            int x = -1, y = -1;

            while (!(x > 0 && x <= sizeGameArea)) {
                System.out.print("Введите номер ряда: ");
                while (!scan.hasNextInt()) {
                    System.out.println("Ошибка данных.");
                    scan.next();
                    System.out.print("\nВведите номер ряда: ");
                }
                x = scan.nextInt();
            }
            x--;

            while (!(y > 0 && y <= sizeGameArea)) {
                System.out.print("Введите номер столбца: ");
                while (!scan.hasNextInt()) {
                    System.out.println("Ошибка данных.");
                    scan.next();
                    System.out.print("\nВведите номер столбца: ");
                }
                y = scan.nextInt();
            }
            y--;

            if (gameArea[x][y] == '1') {
                gameArea[x][y] = 'X';
                printArea(gameArea, sizeGameArea);
                won = true;
            } else {
                gameArea[x][y] = '*';
            }
            countTry++;
        } while (!won);
        System.out.println("\nВы победили! Количество попыток: " + countTry);
    }

    public static void printArea(char[][] area, int sizeGameArea) {
        System.out.println();

        for (int i = 0; i <= sizeGameArea; i++) {
            System.out.print(i + " | ");
        }

        System.out.println();

        for (int i = 0; i < sizeGameArea; i++) {
            System.out.print(i + 1 + " |");
            for (int j = 0; j < sizeGameArea; j++) {
                System.out.print(" " + (area[i][j] == '1' ? "-" : area[i][j]) + " |");
            }
            System.out.println();
        }
    }

}
