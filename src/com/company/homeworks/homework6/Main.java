package com.company.homeworks.homework6;

import com.company.homeworks.homework6.entity.Family;
import com.company.homeworks.homework6.entity.Human;
import com.company.homeworks.homework6.entity.Pet;
import com.company.homeworks.homework6.enums.DayOfWeek;
import com.company.homeworks.homework6.enums.Species;

public class Main {

    public static void main(String[] args) {
        Human father = new Human("Петр", "Петров", (short) 1996);
        Human mother = new Human("Ульяна", "Петрова", (short) 1997);
        Human child = new Human("Вадим", "Петров", (short) 2010, mother, father);

        String[] petHabitsArray = {"1 habit", "2 habit"};

        Pet pet = new Pet(Species.DOG, "Шарик", (short) 13, (short) 67, petHabitsArray);

        Human father1 = new Human("Петр", "Петров", (short) 1996);
        Human mother1 = new Human("Ульяна", "Петрова", (short) 1997);

        String[][] schedule = new String[][]{{DayOfWeek.MONDAY.toString(), "Задача на понедельник"},
                {DayOfWeek.TUESDAY.name(), "Задача на вторник"},
                {DayOfWeek.WEDNESDAY.name(), "Задача на среду"},
                {DayOfWeek.THURSDAY.name(), "Задача на четверг"},
                {DayOfWeek.FRIDAY.name(), "Задача на пятницу"},
                {DayOfWeek.SATURDAY.name(), "Задача на субботу"},
                {DayOfWeek.SUNDAY.name(), "Задача на воскресенье"}};

        Human child2 = new Human("Вадим", "Пfwrwrqw", (short) 2010, (short) 80, mother1, father1, schedule);

        System.out.println(child2);

        Family family = new Family(mother, father);

        family.addChild(child2);
        family.addChild(child);

        family.deleteChild(0);
//        System.out.println(family.deleteChild(1));

//        System.out.println(family);

        // Создание 100 000 экземпляров класса Human
//        for (int i = 0; i < 100000; i++) {
//            Human human = new Human();
//        }

    }
}
