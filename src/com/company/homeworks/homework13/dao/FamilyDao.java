package com.company.homeworks.homework13.dao;

import java.util.List;

public interface FamilyDao<F> {
    List<F> getAllFamilies();

    F getFamilyByIndex(int index);

    boolean deleteFamily(int index);

    boolean deleteFamily(F family);

    boolean saveFamily(F family);

    boolean saveData(List<F> families);

    void loadData();
}
