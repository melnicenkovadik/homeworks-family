package com.company.homeworks.homework13.dao;

import com.company.homeworks.homework13.entity.Family;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao<Family> {
    private List<Family> familyCollection = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return new ArrayList<>(this.familyCollection);
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return this.familyCollection.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        int size = this.familyCollection.size();
        if (index >= 0 && index < size) {
            this.familyCollection.remove(index);
        }

        return size > this.familyCollection.size();
    }

    @Override
    public boolean deleteFamily(Family family) {
        return this.familyCollection.remove(family);
    }

    @Override
    public boolean saveFamily(Family family) {
        if (this.familyCollection.contains(family)) {
            this.familyCollection.set(this.familyCollection.indexOf(family), family);
        } else {
            this.familyCollection.add(family);
        }
        return true;
    }

    public boolean saveData(List<Family> families) {
        try (ObjectOutputStream ous = new ObjectOutputStream(new FileOutputStream("db.txt"))) {
            families.forEach((u) -> {
                try {
                    ous.writeObject(u);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            return true;
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public void loadData() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("db.txt"))) {
            while (true) {
                try {
                    this.saveFamily((Family) ois.readObject());
                } catch (EOFException | ClassNotFoundException e) {
                    break;
                }
            }
        } catch (IOException e) {
            System.out.println("\nДанных нет :(");
        }
    }
}
