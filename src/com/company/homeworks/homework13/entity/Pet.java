package com.company.homeworks.homework13.entity;

import com.company.homeworks.homework13.enums.Species;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public abstract class Pet implements Serializable {
    private static final long serialVersionUID = 12323534643643565L;

    private Species species = Species.UNKNOWN;
    private String nickname;
    private short age;
    private short trickLevel;
    private Set<String> habits = new HashSet<>();

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public abstract void respond();

    public void setSpecies(Species species) {
        this.species = species;
    }

    public Species getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public short getAge() {
        return age;
    }

    public short getTrickLevel() {
        return trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }


    public Pet(String nickname) {
        this.nickname = nickname;
    }

    public Pet(String nickname, short age, short trickLevel, Set<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age &&
                trickLevel == pet.trickLevel &&
                species == pet.species &&
                Objects.equals(nickname, pet.nickname) &&
                Objects.equals(habits, pet.habits);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel, habits);
    }

    @Override
    protected void finalize ( ) {
        System.out.println(this);
    }

    public String prettyFormat() {
        return "Pet{" +
                "species=" + species +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits.toString() +
                '}';
    }


    @Override
    public String toString() {
        return "Pet{" +
                "species=" + species +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits.toString() +
                '}';
    }
}
