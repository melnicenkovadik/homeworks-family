package com.company.homeworks.homework13.exceptions;

public class FamilyOverflowException extends RuntimeException {
    public FamilyOverflowException() {
        super("В семье максимольное количество человек.");
    }
}
