package com.company.homeworks.homework7.enums;

public enum Species {
    DOMESTIC_CAT("Domestic Cat"),
    FISH("Fish"),
    DOG("Dog"),
    ROBOCAT("RoboCat"),
    UNKNOWN("UNKNOWN");


    private String nameSpecies;

    Species(String stringSpecies) {
        this.nameSpecies = stringSpecies;
    }

    public String nameSpec() { return this.nameSpecies; };
}
