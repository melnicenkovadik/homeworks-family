package com.company.homeworks.homework7.entity;

import com.company.homeworks.homework7.enums.Species;

public class Fish extends Pet {

    public Fish(String nickname) {
        super(nickname);
        this.setSpecies(Species.FISH);
    }

    public Fish(String nickname, short age, short trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.FISH);
    }

    public Fish() {
        this.setSpecies(Species.FISH);
    }

    @Override
    public void respond() {
        System.out.println("Буль буль, буль буль");
    }
}
