package com.company.homeworks.homework7.entity;

public final class Man extends Human {
    public Man(String name, String surname, short year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, short year, Human mother, Human father) {
        super(name, surname, year, mother, father);
    }

    public Man(String name, String surname, short year, short iq, Human mother, Human father, String[][] schedule) {
        super(name, surname, year, iq, mother, father, schedule);
    }

    public Man() {
    }

    @Override
    public void greetPet() {
        if (this.getFamily().getPet() != null) {
            System.out.println("MEEEAAAOUUUU, " + this.getFamily().getPet().getNickname());
        } else {
            System.out.println("В Вашей семье нет питомца! :(");
        }
    }

    public void repairCar() {
        System.out.println("Мужик сказал - мужик сломал.");
    }
}
